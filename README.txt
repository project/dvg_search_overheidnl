DvG Search Overheid.nl
-----------------------------

Overview:
---------

This module is used to cache the search results for publications on Overheid.nl
so they can be added to a custom search index and be searched in the website.

Results for the enabled collections are fetched with cron and stored in the database
using a custom entity 'overheidnl_publication'. The cron is scheduled to run overnight, to minimize the impact
on the site and search requests by users during the day.

Documentation on the services of overheid.nl can be found on https://www.koopoverheid.nl:
https://www.koopoverheid.nl/binaries/koop/documenten/instructies/2018/03/24/handleiding-open-data-webservice-van-overheid.nl---sru/Handleiding+voor+het+bevragen+van+de+zoekdienst+via+SRU.pdf


Used variables:
---------------

dvg_search_overheidnl_creator :     Name of the creator of the publications, usually the name
                                    of a municipality or city. This variable can be set by the
                                    administrator on the Overheid.nl search settings page:
                                    admin/config/search/overheidnl

