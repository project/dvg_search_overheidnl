<?php

/**
 * @file
 */

/**
 * Class SearchOverheidnlService.
 *
 * Provides a service to use the overheid.nl search api.
 */
abstract class SearchOverheidnlClient {

  // Search types.
  const SEARCH_OVERHEID = 'overheidnl';
  const SEARCH_BEKENDMAKINGEN = 'bekendmakingen';
  const SEARCH_DISABLED = NULL;

  /**
   * Default search sources.
   *
   * @var array
   */
  private static $searchSources = array(
    'cvdr'  => self::SEARCH_OVERHEID,
    'bm'    => self::SEARCH_OVERHEID,
    'vg'    => self::SEARCH_OVERHEID,
    'oo'    => self::SEARCH_DISABLED,
    'sc'    => self::SEARCH_DISABLED,
    'lnk'   => self::SEARCH_DISABLED,
    'oep'   => self::SEARCH_BEKENDMAKINGEN,
  );

  /**
   * Array with endpoints per search type.
   *
   * @var array
   */
  private static $endpoints = array(
    self::SEARCH_OVERHEID => 'https://zoekdienst.overheid.nl/sru/Search',
    self::SEARCH_BEKENDMAKINGEN => 'https://zoek.officielebekendmakingen.nl/sru/Search',
  );

  /**
   * Collection source to search in.
   *
   * @var string
   */
  protected $searchSource;

  /**
   * Search term for the search query. When empty, all results will be returned.
   *
   * @var string
   */
  protected $searchTerm = '';

  /**
   * Result page size.
   *
   * @var int
   */
  private $pageSize = 100;

  /**
   * Total number of pages.
   *
   * @var int
   */
  private $totalPages = 0;

  /**
   * Total number of records found.
   *
   * @var int
   */
  private $numberOfRecords = 0;

  /**
   * Name of the municipality.
   *
   * @var string
   */
  protected $creator;

  /**
   * Array containing the fetched result data.
   *
   * @var array
   */
  protected $resultData = array();

  /**
   * Flag to check if the page is already fetched or not.
   *
   * @var bool
   */
  private $fetched = FALSE;

  /**
   * SearchOverheidnlClient constructor.
   *
   * @param string $searchSource
   *   Source to search in, needs to be one of the enabled sources.
   * @param string $searchTerm
   *   Term to search for, when empty, all records will be returned.
   */
  public function __construct($searchSource, $searchTerm = '', $pageSize = NULL) {

    if (!array_key_exists($searchSource, self::$searchSources)) {
      throw new Exception('Invalid source type supplied');
    }

    $this->searchSource = $searchSource;
    $this->searchTerm = $searchTerm;

    if (!is_null($pageSize)) {
      $this->pageSize = $pageSize;
    }

    // The name of the municipality will be used as search term.
    $this->creator = variable_get('dvg_search_overheidnl_creator');
    if (is_null($this->creator)) {
      throw new Exception('No creator supplied');
    }
  }

  /**
   * Get all enabled sources.
   *
   * @return array
   *   All enabled search sources.
   */
  public static function getSources() {
    $sources = array();

    // Loop through all types and return the enabled types.
    foreach (self::$searchSources as $source => $service) {
      if ($service != self::SEARCH_DISABLED) {
        $sources[] = $source;
      }
    }

    return $sources;
  }

  /**
   * Get the source collection of this client.
   *
   * @return string
   *   The code of the source collection of this client.
   */
  public function getSource() {
    return $this->searchSource;
  }

  /**
   * Get the searchresults for the supplied page.
   *
   * @param int $page
   *   The page number to fetch.
   *
   * @return array
   *   An array containing all elements found on the supplied page.
   */
  public function getResult($page = 1) {

    if (!isset($this->resultData[$page])) {
      $this->fetchPage($page);
    }

    return $this->resultData[$page];
  }

  /**
   * Return the total number of pages.
   *
   * @return int
   *   Total number of pages, if any found.
   */
  public function getTotalPages() {

    // Make sure at leat a page is fetched.
    if ($this->totalPages == 0 && !$this->fetched) {
      $this->fetchPage(1);
    }

    return $this->totalPages;
  }

  /**
   * Fetch a single page of the searchresult and store in the resultData array.
   *
   * @param int $page
   *   Page number to fetch.
   *
   * @return bool
   *   Return TRUE if records are found.
   */
  protected function fetchPage($page) {
    $searchValue = $this->searchTerm;

    // If no searchvalue is set, make sure ''
    // is used as search value to return all results.
    if (empty($searchValue)) {
      $searchValue = "";
    }

    // Get the right endpoint url.
    $searchType = self::$searchSources[$this->searchSource];
    $searchUrl = self::$endpoints[$searchType];

    // If page is 0 or 1, start on the firstpage.
    $start_record = 1;
    if ($page > 1) {
      // Calculate the starting record.
      $start_record = (($page - 1) * $this->pageSize) + 1;
    }

    $cql_query = '(creator=' . $this->creator . ')and(keyword="' . rawurlencode($searchValue) . '")';

    $searchParameters = array(
      'version' => '1.2',
      'operation' => 'searchRetrieve',
      'x-connection' => $this->searchSource,
      'startRecord' => $start_record,
      'maximumRecords' => $this->pageSize,
      'query' => $cql_query,
    );

    $search_request = $searchUrl . '?' . http_build_query($searchParameters);

    $ignore_ssl_verification = variable_get('dvg_search_overheidnl_ignore_ssl', 0);
    $cafile = variable_get('dvg_search_overheidnl_cafile', '');

    $context_options = NULL;

    if ($ignore_ssl_verification) {
      $context_options = array(
        'ssl' => array(
          'verify_peer' => FALSE,
          'verify_peer_name' => FALSE,
        ),
      );
    }
    elseif (!empty($cafile)) {
      $context_options = array(
        'ssl' => array(
          'cafile' => $cafile,
          'verify_peer' => TRUE,
          'verify_peer_name' => TRUE,
        ),
      );
    }

    $context = $context_options ? stream_context_create($context_options) : NULL;
    $xml_string = file_get_contents($search_request, NULL, $context);

    if (empty($xml_string)) {
      return FALSE;
    }

    $xml_result = new SimpleXMLElement($xml_string);

    $this->numberOfRecords = (int) $xml_result->numberOfRecords;

    // Calculate the number of pages.
    $this->totalPages = (int) ceil($this->numberOfRecords / $this->pageSize);

    // Make the page in the result data array.
    $this->resultData[$page] = array();

    // Add the records to the result array.
    if ($this->numberOfRecords > 0) {
      foreach ($xml_result->records->children() as $record) {
        $recordData = $this->parseRecordXmlData($record->recordData);
        $recordData['metadata'] = implode("\n", $recordData['metadata']);

        // Add the record to te result page.
        $this->resultData[$page][] = $recordData;
      }
    }

    // Update the fetched page.
    $this->fetched = TRUE;

    // Return true if records are found.
    return (count($this->resultData[$page]) > 0);
  }

  /**
   * Parse the Xml record data into an array.
   *
   * Because the structure of the response message are different per collection,
   * each collection should have an own parse function to make sur the right
   * field are parsed and mapped using the data,
   * for example saving it in the database.
   *
   * @param \SimpleXMLElement $recordData
   *   Search result data in XML format.
   *
   * @return array
   *   An array containing all relevant fields for the record.
   */
  protected abstract function parseRecordXmlData(SimpleXMLElement $recordData);

}
