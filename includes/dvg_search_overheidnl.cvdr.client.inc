<?php

/**
 * @file
 */

/**
 * Class SearchOverheidnlCvdrClient.
 *
 * Provides specific functions for the CVDR collection of Overheid.nl.
 */
class SearchOverheidnlCvdrClient extends SearchOverheidnlClient {

  /**
   * SearchOverheidnlCvdrClient constructor.
   *
   * @param string $searchTerm
   *   Word(s) to search for, if left empty, all results will be fetched.
   * @param string $pageSize
   *   Page size for the search.
   */
  public function __construct($searchTerm = '', $pageSize = NULL) {
    parent::__construct('cvdr', $searchTerm, $pageSize);
  }

  /**
   * Parse the record from the search result.
   *
   * @param \SimpleXMLElement $recordDataXml
   *   Get the record data from the xml element.
   */
  protected function parseRecordXmlData(SimpleXMLElement $recordDataXml) {

    $recordData = array(
      'source' => $this->searchSource,
    );
    $recordDataXml->registerXPathNamespace('dcterms', 'http://purl.org/dc/terms/');
    $recordDataXml->registerXPathNamespace('overheidrg', 'http://standaarden.overheid.nl/cvdr/terms/');

    // Walk through all found fields and append them to the record data array.
    // Elements for both namespaces need to be processed.
    foreach ($recordDataXml->xpath('.//dcterms:*|.//overheidrg:*') as $element) {
      $element_name = $element->getName();
      $element_value = (string) $element;

      switch ($element_name) {
        // These fields are stored as searchable/displayable fields.
        case 'title':
        case 'language':
        case 'alternative':
        case 'subject':
          $recordData[$element_name] = $element_value;
          break;

        case 'modified':
          // Save the date modified as a timestamp.
          $recordData[$element_name] = strtotime($element_value);
          break;

        // Store all other values in the metadata field.
        default:
          $recordData['metadata'][$element_name] = $element_value;
          break;

      }
    }

    // Use the field 'alternative' as subject field if set.
    if (!empty($recordData['alternative'])) {
      $recordData['subject'] = $recordData['alternative'];
      unset($recordData['alternative']);
    }

    // Add the url to the record data array.
    $recordData['url'] = (string) $recordDataXml->gzd->enrichedData->publicatieurl_xhtml;

    return $recordData;
  }

}
